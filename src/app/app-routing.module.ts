import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CustomLayoutComponent } from './templates/custom-layout/custom-layout.component';

const routes: Routes = [
  {
    path: '',
    component: CustomLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./templates/inicio/inicio.module').then(m => m.InicioModule)
      },
      {
        path: 'usuarios',
        loadChildren: () => import('./templates/usuarios/usuarios.module').then(m => m.UsuariosModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'enabled',
      relativeLinkResolution: 'corrected',
      anchorScrolling: 'enabled'
    }
  )],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}
