import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio.component';
import { CanActivateGuard } from './guard/can-activate.guard';


const routes: Routes = [
  {
    path: '',
    canActivate: [CanActivateGuard],
    component: InicioComponent,
    data: {
      expectedRole: 'admin',
      grantaccess: ['canAccessInicio']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanActivateGuard]
})
export class InicioRoutingModule {
  static components = [
    InicioComponent
  ];
}
