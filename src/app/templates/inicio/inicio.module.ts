import { NgModule } from '@angular/core';
import { InicioRoutingModule } from './inicio-routing.module';
import { InicioComponent } from './inicio.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [InicioComponent],
  imports: [
    InicioRoutingModule,
    SharedModule
  ]
})
export class InicioModule { }
