import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './usuarios.component';
import { CanActivateGuard } from './guard/can-activate.guard';


const routes: Routes = [
  {
    path: '',
    canActivate: [CanActivateGuard],
    component: UsuariosComponent,
    data: {
      expectedRole: 'admin',
      grantaccess: ['canAccessUsuarios']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanActivateGuard]
})
export class UsuariosRoutingModule {
  static components = [
    UsuariosComponent
  ]
}
