import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { scaleIn400ms } from '../../../@vex/animations/scale-in.animation';
import { fadeInRight400ms } from '../../../@vex/animations/fade-in-right.animation';
import { stagger40ms } from '../../../@vex/animations/stagger.animation';
import { fadeInUp400ms } from '../../../@vex/animations/fade-in-up.animation';
import { scaleFadeIn400ms } from '../../../@vex/animations/scale-fade-in.animation';
import { TableColumn } from '../../../@vex/interfaces/table-column.interface';
import { IUsuario } from '../../shared/models/iusuario';
import { MatTableDataSource } from '@angular/material/table';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { IUsuarioService } from '../../core/services/iusuario.service';

import icContacts from '@iconify/icons-ic/twotone-contacts';
import icSearch from '@iconify/icons-ic/search';
import icFilterList from '@iconify/icons-ic/filter-list';
import icMoreHoriz from '@iconify/icons-ic/more-horiz';
import icAdd from '@iconify/icons-ic/add';
import icEdit from '@iconify/icons-ic/edit';
import icDelete from '@iconify/icons-ic/delete';


@Component({
  selector: 'vex-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
  animations: [
    scaleIn400ms,
    fadeInRight400ms,
    stagger40ms,
    fadeInUp400ms,
    scaleFadeIn400ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class UsuariosComponent implements OnInit {

  @Input() usuarios: IUsuario[];
  displayedColumns: string[] = ['nombre', 'usuario', 'email', 'activado', 'actions'];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<IUsuario> | null;

  icContacts = icContacts;
  icSearch = icSearch;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icAdd = icAdd;
  icEdit = icEdit;
  icDelete = icDelete;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private usuarioService: IUsuarioService) {
  }

  ngOnInit() {
    this.getUsuarios();
  }

  getUsuarios() {
    this.usuarioService.getUsuarios()
      .subscribe((response: IUsuario[]) => {
        this.usuarios = response;
        this.dataSource = new MatTableDataSource<IUsuario>(response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  createUsuario() {

  }

  updateUsuario(usuario: IUsuario) {
    console.log(usuario);
  }

  deleteUsuario(usuario: IUsuario) {

  }

}
