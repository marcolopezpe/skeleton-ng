import { NgModule } from '@angular/core';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';
import { SharedModule } from '../../shared/shared.module';
import { BreadcrumbsModule } from '../../../@vex/components/breadcrumbs/breadcrumbs.module';

@NgModule({
  declarations: [UsuariosComponent],
  imports: [
    UsuariosRoutingModule,
    SharedModule
  ]
})
export class UsuariosModule { }
