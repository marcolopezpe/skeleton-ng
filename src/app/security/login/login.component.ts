import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { fadeInUp400ms } from '../../../@vex/animations/fade-in-up.animation';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';
import { ValidationService } from '../../core/services/validation.service';
import { IUserLogin } from '../../shared/models/iuser-login';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'vex-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    fadeInUp400ms
  ]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  inputType = 'password';
  visible = false;
  errorMessage: string;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private changeDetector: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private authService: AuthService) {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  getErrorMessage(control) {
    return control.invalid ? ValidationService.getValidatorErrorMessage(Object.keys(control.errors)[0]) : '';
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.changeDetector.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.changeDetector.markForCheck();
    }
  }

  submit({value, valid}: {value: IUserLogin, valid: boolean}) {
    const config = new MatSnackBarConfig();
    config.duration = 3000;

    this.authService.login(value)
      .subscribe(() => {

        if (this.authService.isAuthenticated()) {
          config.panelClass = ['messageInformation'];
          this.snackbar.open('Bienvenido!', null, config);

          if (this.authService.redirectUrl) {
            const redirectUrl = this.authService.redirectUrl;
            this.authService.redirectUrl = '';
            this.router.navigate([redirectUrl]);
          } else {
            this.router.navigate(['/']);
          }

        }
      }, error => {
        let loginError = '';

        if (error.status == 400) {
          loginError = 'Usuario y/o contrase\u00F1a incorrecta';
        } else {
          loginError = 'Ha ocurrido un error en el servidor';
        }
        this.errorMessage = loginError;

        config.panelClass = ['messageDanger'];
        this.snackbar.open(loginError, null, config);
      });
  }
}
