export class AppUserAuth {
  constructor() {
    this.isAuthenticated = false;
    this.bearerToken = '';
  }
  nombre: string;
  usuario: string;
  email: string;
  activado: boolean;
  bloqueado: boolean;
  contrasenaExpirada: boolean;
  bearerToken: string;
  isAuthenticated: boolean;
}
