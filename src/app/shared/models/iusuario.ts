import { IRol } from './irol';

export interface IUsuario {
  id: number;
  nombre: string;
  usuario: string;
  email: string;
  activado: boolean;
  roles: IRol[];
}
