import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IconModule } from '@visurel/iconify-angular';
import { ContainerModule } from '../../@vex/directives/container/container.module';
import { MaterialModule } from './material/material.module';
import { PageLayoutModule } from '../../@vex/components/page-layout/page-layout.module';
import { BreadcrumbsModule } from '../../@vex/components/breadcrumbs/breadcrumbs.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    FormsModule,
    FlexLayoutModule,
    IconModule,
    ContainerModule,
    MaterialModule,
    CommonModule,
    PageLayoutModule,
    BreadcrumbsModule
  ]
})
export class SharedModule {
}
