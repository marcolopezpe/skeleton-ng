import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.match('.+/oauth/(token|revoke)$')) {
      let changedRequest = request.clone(
        {
          headers: new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.authService.appUserAuth.bearerToken)
            .set('Content-Type', 'application/json')
        });

      return next.handle(changedRequest);
    } else {
      return next.handle(request);
    }
  }
}
