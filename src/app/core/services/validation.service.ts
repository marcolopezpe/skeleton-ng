import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  static getValidatorErrorMessage(code: string) {
    const config = {
      required: 'Requerido'
    };
    return config[code];
  }
}
