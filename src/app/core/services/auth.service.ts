import { EventEmitter, Injectable, Output } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AppUserAuth } from '../../shared/models/app-user-auth';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { IUserLogin } from '../../shared/models/iuser-login';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = `${environment.host}`;
  authUrl = this.baseUrl + '/oauth';
  redirectUrl: string;
  appUserAuth = new AppUserAuth();

  private jwtHelper = new JwtHelperService();

  @Output() authChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private http: HttpClient,
    private router: Router) {
    this.appUserAuth = this.getUserLoggedIn();
    this.setUserLoggedIn(this.appUserAuth);
  }

  private userAuthChanged(status: boolean) {
    this.authChanged.emit(status); // Raise changed event
  }

  login(userLogin: IUserLogin): Observable<AppUserAuth> {
    const body = `grant_type=password&username=${encodeURIComponent(userLogin.username)}&password=${encodeURIComponent(userLogin.password)}`;

    return this.http.post(this.authUrl + '/token', body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
        .set('Authorization', 'Basic ' + btoa(environment.client_id + ':' + environment.client_secret))
    })
      .pipe(
        map((loggedIn: any) => {
          const appUserAuth = new AppUserAuth();
          const bearerToken = loggedIn.access_token;
          appUserAuth.bearerToken = bearerToken;
          appUserAuth.isAuthenticated = true;
          appUserAuth.nombre = this.jwtHelper.decodeToken(bearerToken)?.nombre;
          appUserAuth.usuario = this.jwtHelper.decodeToken(bearerToken)?.usuario;
          appUserAuth.email = this.jwtHelper.decodeToken(bearerToken)?.email;
          appUserAuth.activado = this.jwtHelper.decodeToken(bearerToken)?.activado;
          appUserAuth.bloqueado = this.jwtHelper.decodeToken(bearerToken)?.bloqueado;
          appUserAuth.contrasenaExpirada = this.jwtHelper.decodeToken(bearerToken)?.contrasenaExpirada;
          this.setUserLoggedIn(appUserAuth);

          return appUserAuth;
        }),
        catchError(AuthService.handleError)
      );
  }

  logout(): Observable<boolean> {
    console.log('### logout auth service...');
    return this.http.post<boolean>(this.authUrl + '/revoke', this.getUserLoggedIn().bearerToken)
      .pipe(
        map(loggedOut => {
          const appUserAuth = new AppUserAuth();
          this.setUserLoggedIn(appUserAuth);
          return loggedOut;
        }),
        catchError(AuthService.handleError)
      );
  }

  private static handleError(error: HttpErrorResponse) {
    console.error('server error: ', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
    }
    return throwError(error || 'Server error');
  }

  setUserLoggedIn(appUserAuth: AppUserAuth) {
    sessionStorage.setItem('currentUser', JSON.stringify(appUserAuth));

    this.appUserAuth = appUserAuth;
    this.userAuthChanged(this.isAuthenticated());
  }

  isAuthenticated() {
    const isExpired = this.jwtHelper.isTokenExpired(this.getUserLoggedIn().bearerToken);
    return this.getUserLoggedIn().isAuthenticated && !isExpired;
  }

  getUserLoggedIn(): AppUserAuth {
    let appUserAuth = JSON.parse(sessionStorage.getItem('currentUser'));
    appUserAuth = appUserAuth || new AppUserAuth();
    return appUserAuth;
  }
}
