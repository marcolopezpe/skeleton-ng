import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { IUsuario } from '../../shared/models/iusuario';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IUsuarioService {

  baseUrl = `${environment.host}`;
  usuariosBaseUrl = this.baseUrl + '/api/usuarios';

  constructor(
    private http: HttpClient) {
  }

  getUsuarios(): Observable<IUsuario[]> {
    return this.http.get<IUsuario[]>(this.usuariosBaseUrl)
      .pipe(
        map(usuarios => {
          return usuarios;
        }),
        catchError(IUsuarioService.handleError)
      );
  }

  private static handleError(error: HttpErrorResponse) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
      // Use the following instead if using lite-server
      // return Observable.throw(err.text() || 'backend server error');
    }
    return throwError(error || 'server error');
  }
}
